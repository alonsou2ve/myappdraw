import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { FeaturedRoutingModule } from "./featured-routing.module";
import { FeaturedComponent } from "./featured.component";
import { Funcionalidad1Component } from "../funcionalidad1/funcionalidad1.component";
import { Funcionalidad2Component } from "../funcionalidad2/funcionalidad2.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        FeaturedRoutingModule
        
    ],
    declarations: [
        FeaturedComponent, Funcionalidad1Component, Funcionalidad2Component ],

    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class FeaturedModule { }
