import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { FeaturedComponent } from "./featured.component";
import { Funcionalidad1Component } from "../funcionalidad1/funcionalidad1.component";
import { Funcionalidad2Component } from "../funcionalidad2/funcionalidad2.component";


const routes: Routes = [
   { path: "", component: FeaturedComponent },
   { path: "funcionalidad1", component: Funcionalidad1Component },
   { path: "funcionalidad2", component: Funcionalidad2Component}
];
@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule],

})

export class FeaturedRoutingModule { }
