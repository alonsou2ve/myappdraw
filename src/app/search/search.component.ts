import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store, State } from "@ngrx/store";
import * as Toast from 'nativescript-toasts';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.models";
import * as dialogs from "tns-core-modules/ui/dialogs";
import { timestamp } from "rxjs/operators";
import { Color, View } from "tns-core-modules/ui/core/view/view"
import { AppState } from "../app.module";
import { disableDebugTools } from "@angular/platform-browser";
import * as SocialShare from "nativescript-social-share";


@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    styleUrls: ["~/../search.css"]
})

export class SearchComponent implements OnInit {

    constructor(private noticias: NoticiasService,
        private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }

    resultados: Array<string> = [];

    @ViewChild("layout", null) layout: ElementRef;

    ngOnInit(): void {
        // Init your component properties here.
        /*  this.noticias.agregar("Hola 1");
          this.noticias.agregar("Hola 2");
          this.noticias.agregar("Hola 3");
  
          this.resultados.push("Hola 1");
          this.resultados.push("Hola 2");
          this.resultados.push("Hola 3");  Evaluacion Modulo I y II */

        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({ text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT });
                }
            });

    }

    onPull(arg) {
        let contador = this.resultados.length + 1;
        let saludos = "Hola " + contador;

        console.log(arg);
        const pullRefresh = arg.object;
        setTimeout(() => {
            this.resultados.push(saludos);
            pullRefresh.refreshing = false;
        }, 1000);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    onItemTap(arg): void {
        /* console.dir(x); */
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(arg.view.bindingContext)));
    }


    onLongPress(s) {
        console.log(s);
        SocialShare.shareText(s, "Asunto: Compartido desde el curso!");
      }

    onDelete(item): void {
        this.resultados.splice(item, 1);
        alert('Se elimino el item ' + item);

    }

    onDetalle(item): void {
        alert('Mostrar los detalles del elemento ' + item);
    }



    buscarAhora(s: string) {
        console.dir("buscarAhora"+s);
        this.noticias.buscar(s).then((r:any) => {
            console.log("resultados buscarAhora "+ JSON.stringify(r));
            this.resultados = r;
        }, (e)=> {
            console.log("Error buscarAhora "+e);
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT})
        });
        
        /* this.resultados = this.resultados.filter((x) => x.indexOf(s) >= 0);
        console.log(s);
        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 300,
            delay: 150
        })); */

    }

}
